# Malabi Dyslexias Identification

Scripts for 'Error_Response_Coding' reader's Malabi test responses, 'Norming' a population of responses, and 'Dyslexias_Identification'. Data is is only uploaded once made anonymous.